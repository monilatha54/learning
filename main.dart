// Import From Global
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:scramble/config/remote/authentication/authentication_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:scramble/localization/locale_constant.dart';
import 'package:scramble/localization/localizations_delegate.dart';
import 'package:scramble/config/remote/user_repository.dart';
import 'package:scramble/screens/common/update.dart';
import 'package:scramble/screens/common/widgets/update_modal.dart';
import 'package:scramble/screens/features/signin/signin_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get_storage/get_storage.dart';

// For Marketing
import 'package:firebase_analytics/firebase_analytics.dart';

// Import From Pub.dev
import 'package:provider/provider.dart';

// Import the firebase_app_check plugin

// Import From Local
import 'package:scramble/screens/features/tabscreen/tabscreen.dart';

// For Testing
import 'package:scramble/screens/onboarding.dart';

import 'config/theme.dart';

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    'This channel is used for important notifications.', // description

    importance: Importance.high,
    playSound: true);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

bool isUpdateAvail = false;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  await FirebaseMessaging.instance.subscribeToTopic('Allusers');

  // debugPaintSizeEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale newLocale) {
    var state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(newLocale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseAnalytics analytics = FirebaseAnalytics();

  Locale _locale;

  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  // @override
  // void didChangeDependencies() async {
  //   getLocale().then((locale) {
  //     setState(() {
  //       _locale = locale;
  //     });
  //   });
  //   super.didChangeDependencies();
  // }

  @override
  Widget build(BuildContext context) {
    if (kReleaseMode) {
      analytics.setAnalyticsCollectionEnabled(true);
    } else {
      analytics.setAnalyticsCollectionEnabled(false);
    }
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Scramble',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          backgroundColor: AppColors.primaryBackgroundNew,
          iconTheme: IconThemeData(color: Colors.white.withOpacity(0.1)),
        ),
        tabBarTheme: TabBarTheme(
          labelColor: Colors.white,
          unselectedLabelColor: Colors.white.withOpacity(0.25),
          labelStyle: GoogleFonts.spaceGrotesk(
              fontSize: 14,
              // textAlign: TextAlign.center,
              fontWeight: FontWeight.w700,
              color: Colors.white),
        ),
        textTheme: GoogleFonts.spaceGroteskTextTheme(
          Theme.of(context).textTheme,
        ),
        primarySwatch: Colors.indigo,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      locale: _locale,
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
      supportedLocales: [Locale('en', ''), Locale('hi', ''), Locale('ta', '')],
      localizationsDelegates: [
        AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale?.languageCode == locale?.languageCode &&
              supportedLocale?.countryCode == locale?.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales?.first;
      },
      home: MultiProvider(
        providers: [
          Provider<AuthenticationService>(
            create: (_) => AuthenticationService(),
          ),
          StreamProvider(
            create: (context) =>
                context.read<AuthenticationService>().authStateChanges,
            initialData: null,
          )
        ],
        child: isUpdateAvail ? UppdateDialoge() : AuthenticationWrapper(),
      ),
    );
  }
}

class AuthenticationWrapper extends StatefulWidget {
  const AuthenticationWrapper({
    Key key,
  }) : super(key: key);

  @override
  _AuthenticationWrapperState createState() => _AuthenticationWrapperState();
}

class _AuthenticationWrapperState extends State<AuthenticationWrapper> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};

  @override
  void initState() {
    _initGoogleMobileAds();
    initPlatformState();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User>();
    if (firebaseUser != null && firebaseUser.emailVerified) {
      // Update device identifier to exists user on app update
      // Get the document and decide it
      updateDeviceInfo();
      return FutureBuilder(
          future: updateRPCurl(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TabLayout(
                selectedIndex: snapshot.data,
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          });
    } else {
      return FutureBuilder(
          future: SharedPreferences.getInstance(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              SharedPreferences prefs = snapshot.data;
              bool firstTime = prefs.getBool('first_time');
              firstTime = firstTime == null ? true : firstTime;
              if (firstTime) {
                prefs.setBool('first_time', false);
                return OnboardingScreen();
              } else {
                return Selectionpage();
              }
            }
            return CircularProgressIndicator();
          });
    }
  }

  Future<void> initPlatformState() async {
    await APPUPDATE().checkupdate();
    Map<String, dynamic> deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

   if (!mounted) return;
    _deviceData = deviceData;
  }


  Future<InitializationStatus> _initGoogleMobileAds() {
    return MobileAds.instance.initialize();
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  updateDeviceInfo() async {
    var deviceName =
        Platform.isAndroid ? _deviceData['model'] : _deviceData['name'];
    var deviceVersion = Platform.isAndroid
        ? _deviceData['version.release']
        : _deviceData['systemVersion'];
    var identifier = Platform.isAndroid
        ? _deviceData['androidId']
        : _deviceData['identifierForVendor'];
    // update for existing user device identifier
    Map<String, String> deviceData = {
      'deviceName': deviceName,
      'deviceVersion': deviceVersion,
      'identifier': identifier
    };

    await Api().getDeviceInfo().then((value) async {
      if (!value) {
        await Api().updateUser(deviceData);
      }
    });
  }

  Future<int> updateRPCurl() async {
    final lStorage = GetStorage();
    String maticRPCURL = lStorage.read('maticRPCURL');
    DocumentSnapshot config =
        await FirebaseFirestore.instance.collection("config").doc('1').get();
    if (maticRPCURL == null) {
      lStorage.write('maticRPCURL', config.data()['maticRPCURL']);
    }
    return config.data()['tabIndex'];
  }
}
